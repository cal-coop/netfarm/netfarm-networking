(in-package :netfarm-server)

(defstruct dependency-vertex
  type dependent dependency)
   
(defclass memory-database-mixin ()
  ((vague-objects :initform (make-chash-table :test 'equal)
                  :reader memory-system-vague-objects)
   (computed-value-tuples :initform (make-chash-table :test 'equal)
                          :reader memory-system-computed-values)
   (presentable-set :initform (make-chash-table :test 'equal)
                    :accessor memory-system-presentable-set)
   (other-interesting-set :initform (make-chash-table :test 'equal)
                          :accessor memory-system-other-interesting-set)
   (affected-by-table :initform (make-chash-table :test 'equal)
                      :reader memory-system-affected-by-table)
   (affecting-table :initform (make-chash-table :test 'equal)
                    :reader memory-system-affecting-table)))

(defclass memory-system (dependency-table-mixin memory-database-mixin
                         netfarm-system)
  ())
(defclass simple-memory-system (dependency-list-mixin memory-database-mixin
                                netfarm-system)
  ())

;;; Vague object storage
(defmethod put-vague-object ((system memory-database-mixin) vague-object)
  (setf (getchash (netfarm:vague-object-name vague-object)
                  (memory-system-vague-objects system))
        vague-object))
(defmethod get-vague-object ((system memory-database-mixin) name)
  (or (getchash name (memory-system-vague-objects system))
      (error 'vague-object-not-found :name name)))

(defmethod map-vague-objects (function (system memory-database-mixin))
  (mapchash (lambda (name vague-object)
              (declare (ignore vague-object))
              (funcall function name))
            (memory-system-vague-objects system)))

(defmethod count-vague-objects ((system memory-database-mixin))
  (chash-table-count (memory-system-vague-objects system)))

(defmethod vague-object-stored-p ((system memory-database-mixin) name)
  (nth-value 1
             (getchash name (memory-system-vague-objects system))))
(defmethod presentable-name-p ((system memory-database-mixin) name)
  (values (getchash name (memory-system-presentable-set system))))
(defmethod (setf presentable-name-p)
    (presentable? (system memory-database-mixin) name)
  (if presentable?
      (setf (getchash name (memory-system-presentable-set system))
            t)
      (remchash name (memory-system-presentable-set system))))

(defmethod count-presentable-objects ((system memory-database-mixin))
  (chash-table-count (memory-system-presentable-set system)))

;;; Other intersting names
(defmethod add-other-interesting-block ((system memory-database-mixin) name)
  (setf (getchash name (memory-system-other-interesting-set system))
        t))
(defmethod remove-other-interesting-block ((system memory-database-mixin) name)
  (remchash name (memory-system-other-interesting-set system)))
(defmethod other-interesting-block-p ((system memory-database-mixin) name)
  (values (getchash name (memory-system-other-interesting-set system))))

;;; Side effects
(defun computed-value-count (system target-name slot-position value)
  (getchash (list target-name slot-position value)
            (memory-system-computed-values system)
            0))
(defun (setf computed-value-count)
    (count system target-name slot-position value)
  (setf (getchash (list target-name slot-position value)
                 (memory-system-computed-values system))
        count))

(defun adjust-computed-value-count
    (system target-name slot-position value change)
  (let (set-value)
    (modify-value ((list target-name slot-position value)
                   (memory-system-computed-values system))
        (value present?)
      (values (setf set-value (if present?
                                  (+ change value)
                                  change))
              t))
    set-value))

(defmethod computed-value-counters ((system memory-database-mixin) name)
  (let ((counters '()))
    (mapchash (lambda (values count)
                (destructuring-bind (target-name slot-position value)
                    values
                  (when (and (string= name target-name)
                             (not (zerop count)))
                    (push (list slot-position value count)
                          counters))))
              (memory-system-computed-values system))
    counters))

(defun add-affecting-edge (system target cause)
  (modify-value
      (cause (memory-system-affected-by-table system))
      (edges present?)
    (values (if present?
                (cons target edges)
                (list target))
            t))
  (modify-value
      (target (memory-system-affecting-table system))
      (edges present?)
    (values (if present?
                (cons cause edges)
                (list cause))
            t)))

(defmethod apply-side-effect ((system memory-database-mixin) cause-name
                              (type (eql 'netfarm-scripts:add-computed-value))
                              &key slot value target)
  (let ((target-name (object-hash system target))
        (value (netfarm:normalise-graph value)))
    ;; Only add a computed value after we've "cancelled out" the remove
    ;; effects.
    (let* ((target (getchash target-name (memory-system-vague-objects system)))
           (slot-position (netfarm:netfarm-slot-position slot)))
      (when (plusp
             (adjust-computed-value-count system
                                          target-name slot-position value
                                          1))
        (push value (svref (netfarm:vague-object-computed-values target)
                           slot-position)))
      (add-affecting-edge system target-name cause-name))))

(defmethod apply-side-effect ((system memory-system) cause-name
                              (type (eql 'netfarm-scripts:remove-computed-value))
                              &key slot value target)
  (let ((target-name (object-hash system target))
        (value (netfarm:normalise-graph value)))
    ;; Drop off a computed value if we have more add effects.
    (let* ((target (getchash target-name (memory-system-vague-objects system)))
           (slot-position (netfarm:netfarm-slot-position slot)))
      (unless (minusp
               (adjust-computed-value-count system
                                            target-name slot-position value
                                            -1))
        (setf (svref (netfarm:vague-object-computed-values target)
                     slot-position)
              (remove-if (lambda (this-value)
                           (netfarm-scripts:netfarm-equal this-value
                                                          value))
                         (svref (netfarm:vague-object-computed-values target)
                                slot-position)
                         :count 1)))
      (add-affecting-edge system target-name cause-name))))

(defmethod objects-affected-by ((system memory-database-mixin) name)
  (values (getchash name (memory-system-affected-by-table system))))

(defmethod objects-affecting ((system memory-database-mixin) name)
  (values (getchash name (memory-system-affecting-table system))))
