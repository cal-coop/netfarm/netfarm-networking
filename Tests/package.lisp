(defpackage :netfarm-network-tests
  (:use :cl :parachute :netfarm)
  (:export #:run-tests #:gitlab-ci-test))
