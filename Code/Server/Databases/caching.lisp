(in-package :netfarm-server)

(defclass caching-mixin ()
  ((cache :reader caching-mixin-cache)
   (cache-visits :initform (decentralise-utilities:box 0)
                 :accessor %caching-mixin-visits)
   (cache-misses :initform 0 :accessor caching-mixin-misses)
   (fallback-vague-object-size :initarg :fallback-vague-object-size
                               :initform 1000
                               :reader caching-mixin-fallback-size)
   (object-locks :initform (decentralise-utilities:make-named-resource-table)
                 :reader caching-mixin-locks)
   (side-effect-lock :initform (bt:make-lock "Side effect application lock")
                     :reader caching-mixin-lock))
  (:documentation "A system that caches vague objects, to lessen the load on a database."))

(defvar *next-method*)
(defun call-bound-method ()
  (funcall *next-method*))
(defmethod initialize-instance :after ((cache caching-mixin) &key (cache-size 100000000))
  (setf (slot-value cache 'cache)
        (cacle:make-cache cache-size
                          (lambda (name)
                            (declare (ignore name))
                            (multiple-value-bind (vague-object size)
                                (call-bound-method)
                              (if (integerp size)
                                  (values vague-object size)
                                  (values vague-object
                                          (caching-mixin-fallback-size cache)))))
                          :test 'equal
                          :policy :lfuda)))
    
(defmethod get-vague-object :around ((system caching-mixin) name)
  (let ((*next-method* (lambda ()
                         (incf (caching-mixin-misses system))
                         (call-next-method))))
    (decentralise-utilities:with-unlocked-box (v (%caching-mixin-visits system))
      (incf v))
    (values
     (decentralise-utilities:with-named-resource (name (caching-mixin-locks system))
       (cacle:cache-fetch (caching-mixin-cache system) name)))))

(defmethod put-vague-object :around ((system caching-mixin) vague-object)
  (let* ((name (netfarm:vague-object-name vague-object))
         (length (call-next-method))
         (*next-method* (lambda ()
                          (if (integerp length)
                              (values vague-object length)
                              (values vague-object
                                      (caching-mixin-fallback-size system))))))
    (decentralise-utilities:with-named-resource (name (caching-mixin-locks system))
      (cacle:cache-fetch (caching-mixin-cache system) name))
    length))

(defmethod apply-side-effect :after ((system caching-mixin) cause type &key)
  "Drop caches after we apply any side effect."
  (declare (ignore type))
  (cacle:cache-remove (caching-mixin-cache system) cause))
