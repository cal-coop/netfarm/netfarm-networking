(in-package :netfarm-network-tests)

(define-test server-client-tests :serial nil)

(defvar *system*)

(defun wait-for-server-to-catch-up (system client
                                    &key minimum-presentable-count
                                         (maximum-wait-time 5.0))
  "Wait for the server to 'catch up' on the test, by waiting until either some time has passed, or until at least some number of objects are presentable." 
  (loop for time from 0 to maximum-wait-time by 0.1
        when (and (not (null minimum-presentable-count))
                  (>= (netfarm-server:count-presentable-objects system)
                      minimum-presentable-count)
                  (>= (decentralise-client:count-known-blocks client)
                      minimum-presentable-count))
          do (return-from wait-for-server-to-catch-up)
        do (sleep 0.1))
  (error "Server didn't catch up?"))

(defun make-test-client ()
  (multiple-value-bind (connection1 connection2)
      (decentralise-connection:make-hidden-socket)
    (decentralise-system:add-connection *system* connection1)
    (make-instance 'netfarm-client:client
                   :bootstrap-connections (list connection2))))

(defmacro with-test-client ((client) &body body)
  `(let ((*system* (make-instance 'netfarm-server:memory-system)))
     (decentralise-system:start-system *system*)
     (let ((,client (make-test-client)))
       (unwind-protect
	    (progn ,@body)
         (decentralise-client:stop-client ,client)
         (decentralise-system:stop-system *system*))
       (values *system* ,client))))

(defclass a ()
  ((a :initarg :a :reader a)
   (b :initarg :b :reader b))
  (:metaclass netfarm:netfarm-class))

(define-test save-full-object
  :parent server-client-tests
  (with-test-client (client)
    (let ((object (make-instance 'a
                                 :a "This is the value for the slot A"
                                 :b "This is the value for the slot B")))
      (netfarm-client:save-object client object)
      (wait-for-server-to-catch-up *system* client
                                   :minimum-presentable-count 1)
      (let ((object* (netfarm-client:find-object client
                                                 (netfarm:bytes->base64
                                                  (netfarm:hash-object object)))))
        (is string= (a object) (a object*))
        (is string= (b object) (b object*))
        (of-type a object*)))))

(define-test save-out-of-order
  :parent server-client-tests
  (with-test-client (client)
    (let* ((object1 (make-instance 'a))
	   (object2 (make-instance 'a :a object1)))
      ;; Save the objects and their schema in the wrong order.
      (netfarm-client:save-single-object client object2)
      (netfarm-client:save-single-object client object1)
      (netfarm-client:save-single-object client
					 (class->schema (find-class 'a)))
      (wait-for-server-to-catch-up *system* client
                                   :minimum-presentable-count 2)
      (true (netfarm-client:find-object client
					(netfarm:bytes->base64
					 (netfarm:hash-object object2)))))))


(netfarm-scripts:define-script *send-self-to-c*
    ("my-c" "here it is!" "receive")
  ;; arguments: (self)
  (:method "initialize" 0)
  ;; (list "here it is!" <self>)
  (get-value 1) self (list 2)
  ;; (object-value self "my-c")
  self (get-value 0) object-value
  (get-value 2)
  ;; (call-method <c> "receive" <list>)
  (call-method 1) return)

(netfarm-scripts:define-script *receive-my-b*
    ("your-b")
  (:method "receive" 1)
  ;; (add-computed-value "your-b" value)
  (get-value 0) (get-env 1 0) add-computed-value (byte 0) return)

(defclass b ()
  ((my-c :initarg :my-c :reader c))
  (:scripts *send-self-to-c*)
  (:metaclass netfarm:netfarm-class))
(defclass b2 ()
  ((my-c :initarg :my-c :reader c)
   (random-slot))
  (:scripts *send-self-to-c*)
  (:metaclass netfarm:netfarm-class))
(defclass c ()
  ((text :initarg :text :reader text)
   (your-b :computed t :reader your-b))
  (:scripts *receive-my-b*)
  (:metaclass netfarm:netfarm-class))

(define-test server-script-machine
  :parent server-client-tests
  (with-test-client (client)
    (let* ((c (make-instance 'c :text "C"))
	   (b (make-instance 'b :my-c c)))
      (netfarm-client:save-object client c :follow-updates? t)
      (netfarm-client:save-object client b)
      (wait-for-server-to-catch-up *system* client
                                   :minimum-presentable-count 4)
      ;; The effect should be propagated back to our old copy of C in a second.
      (sleep 1)
      (destructuring-bind ((text b*))
          (your-b c)
        (is equalp (hash-object b) (hash-object b*))
        (is string= "here it is!" text))
      (let ((c* (netfarm-client:find-object client
				            (netfarm:hash-object* c)
                                            :follow-updates? t)))
	(destructuring-bind ((text b*))
            (your-b c*)
          (is equalp (hash-object b) (hash-object b*))
          (is string= "here it is!" text))
        (netfarm-client:save-object client (make-instance 'b2 :my-c c))
        (sleep 1)
        (true (member (find-class 'b2) (your-b c*)
                      :key (alexandria:compose #'class-of #'second)))))))
