(in-package :netfarm-server)

(defclass dependency-list-mixin ()
  ((dependencies :initform (box '())
                 :accessor memory-system-dependency-list))
  (:documentation "A mixin that implements a dependency graph by storing a list of its edges."))

(defmacro with-dependency-list ((list system) &body body)
  `(with-unlocked-box (,list (memory-system-dependency-list ,system))
     ,@body))

;;; Dependency graph
(defmethod add-dependency-edge
    ((system dependency-list-mixin) type dependent dependency)
  (with-dependency-list (list system)
    (push (make-dependency-vertex :type type
                                  :dependent dependent
                                  :dependency dependency)
          list)))
(defmethod remove-dependents
    ((system dependency-list-mixin) dependency)
  (with-dependency-list (list system)
    (let ((removed '()))
      (setf list
            (loop for edge in list
                  if (string= (dependency-vertex-dependency edge)
                              dependency)
                    do (push (list (dependency-vertex-dependent edge)
                                   (dependency-vertex-type edge))
                             removed)
                  else
                    collect edge))
      removed)))

(defmethod map-dependencies (function (system dependency-list-mixin) type dependent)
  (with-dependency-list (list system)
    (loop for vertex in list
          when (and (string= (dependency-vertex-dependent vertex) dependent)
                    (or (eql type :all)
                        (eql type (dependency-vertex-type vertex))))
            do (funcall function
                        (dependency-vertex-type vertex)
                        (dependency-vertex-dependency vertex)))))
(defmethod map-dependents (function (system dependency-list-mixin) type dependency)
  (with-dependency-list (list system)
    (loop for vertex in list
          when (and (string= (dependency-vertex-dependency vertex) dependency)
                    (or (eql type :all)
                        (eql type (dependency-vertex-type vertex))))
            do (funcall function
                        (dependency-vertex-type vertex)
                        (dependency-vertex-dependent vertex)))))
(defmethod map-dependency-edges (function (system dependency-list-mixin))
  (with-dependency-list (list system)
    (loop for vertex in list
          do (funcall function
                      (dependency-vertex-type vertex)
                      (dependency-vertex-dependent vertex)
                      (dependency-vertex-dependency vertex)))))
