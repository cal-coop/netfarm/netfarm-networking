(in-package :netfarm-server)

(defvar *timeout* 1
  "A timeout for any looping mailbox readers to use, so that they may be stopped gracefully when control is returned to FUNCALL-LOOP.")
(defvar *output-lock* (bt:make-lock)
  "A lock to ensure backtraces printed don't get mangled.")

(defmacro with-received-message ((name mailbox) &body body)
  "Bind NAME to a message received from MAILBOX. (If there isn't a message after *TIMEOUT* seconds, do nothing.)"
  `(let ((,name (safe-queue:mailbox-receive-message ,mailbox :timeout *timeout*)))
     (unless (null ,name)
       ,@body)))

(defun funcall-loop (step-function system)
  "Call the step function repeatedly with the system, until a shutdown is requested."
  (loop until (decentralise-system:system-stopped-p system)
        do (decentralise-utilities:handler-case* (funcall step-function system)
             (error (e)
                (bt:with-lock-held (*output-lock*)
                  (trivial-backtrace:print-backtrace e :output *debug-io*))
                (sleep 5)))))
