(asdf:defsystem :netfarm-network-tests
  :depends-on (:netfarm :netfarm-client :netfarm-server :parachute)
  :components ((:file "package")
               (:file "run-tests")
               (:file "server-client")
               (:file "server-server")
               (:module "Stress-test"
                :components ((:file "package")
                             (:file "data")
                             (:file "stress-test")))))
