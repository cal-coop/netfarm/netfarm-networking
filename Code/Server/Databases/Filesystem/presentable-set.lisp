(in-package :netfarm-server-filesystem-database)

;;; The presentable set is represented using its complement, as we predict that
;;; there will be many more presentable objects than unpresentable objects.
;;; This unpresentable set is then represented the same way the
;;; other-interesting set is represented.

(defmethod (setf netfarm-server:presentable-name-p)
    (presentable? (system filesystem-database-mixin) name)
  (with-unlocked-box (table (filesystem-database-unpresentable-set system))
    (if presentable?
        (remhash name table)
        (setf (gethash name table) t))))

(defmethod netfarm-server:presentable-name-p
    ((system filesystem-database-mixin) name)
  (with-unlocked-box (table (filesystem-database-unpresentable-set system))
    (not (gethash name table))))

(defmethod netfarm-server:put-vague-object :before
    ((system filesystem-database-mixin) vague-object)
  (setf (netfarm-server:presentable-name-p system
                                           (netfarm:vague-object-name vague-object))
        nil))
    

(defgeneric save-unpresentable-set (system)
  (:method ((system filesystem-database-mixin))
    (with-unlocked-box (table (filesystem-database-unpresentable-set system))
      (save-string-set system "unpresentable-set" table))))

(defgeneric load-unpresentable-set (system)
  (:method ((system filesystem-database-mixin))
    (load-string-set system 'unpresentable-set
                     "unpresentable-set" 'unpresentable)))
