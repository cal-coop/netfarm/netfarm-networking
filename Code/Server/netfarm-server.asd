(asdf:defsystem :netfarm-server
  :author "The Cooperative of Applied Language"
  :description "An implementation of the Netfarm server that uses decentralise2"
  :depends-on (:decentralise2 :netfarm :netfarm-decentralise2 :cacle :fset :cl-bloom)
  :components ((:file "package")
               (:file "thread-helpers")
               (:module "Protocol" :depends-on ("package" "thread-helpers")
                :components ((:file "system")
                             (:file "decentralise2")
                             (:file "dependency-graph")
                             (:file "side-effects")))
               (:module "Decentralise2" :depends-on ("Protocol")
                :components ((:file "objects-affecting")
                             (:file "effect")
                             (:file "other-sources")
                             (:file "counters")))
               (:module "Databases" :depends-on ("Protocol")
                :components ((:file "dependency-list")
                             (:file "dependency-table")
                             (:file "memory-database")
                             (:file "caching")
                             (:file "tracing")))
               (:module "Stages" :depends-on ("Protocol" "Databases")
                :components ((:file "system-behaviour")
                             (:file "dependency-resolution")
                             (:file "script-machine-runner")))))
