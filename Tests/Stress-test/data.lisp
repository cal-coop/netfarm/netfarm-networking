(in-package :netfarm-server-stress-test)

;;; A "graph" structure, in which nodes

(define-script *send-self-to-parent* ("parent" "add-child")
  (:method "initialize" 0)
  ;; do I have a parent?
  self (get-value 0) object-boundp
  ;; get out if there is no parent
  (jump-cond 0 3 0 0) (byte 1) return
  ;; send myself to my parent
  self (get-value 0) object-value (get-value 1) (call-method 0) return)

(define-script *add-message-as-child* ("children")
  ;; arguments: (self sender message)
  (:method "add-child" 0)
  (get-value 0) sender add-computed-value (byte 0) return)

(defclass graph ()
  ((parent   :initarg :parent :reader parent)
   (children :computed t :reader children)
   (data     :initarg :data   :reader data))
  (:metaclass netfarm-class)
  (:scripts *send-self-to-parent* *add-message-as-child*))

(defun parent* (graph)
  "Get the eldest parent of the graph."
  (if (slot-boundp graph 'parent)
      (parent* (parent graph))
      graph))

(defun map-nodes (function graph)
  ;; Traverse the children first to make more dependencies.
  (dolist (computed-value (children graph))
    (map-nodes function computed-value))
  (funcall function graph))

(defun graph-size (graph)
  (let ((n 0))
    (map-nodes (lambda (node)
                 (declare (ignore node))
                 (incf n))
               graph)
    n))

(defun binomial-random (n pr)
  "Return a random value from a binomial distribution with N experiments, each with a PR success probability."
  (loop repeat n
        count (< (random 1.0) pr)))

(defun make-graph-node (&optional parent)
  (let* ((data (random (expt 2 128)))
         (node (if (null parent)
                   (make-instance 'graph :data data)
                   (make-instance 'graph :data data :parent parent))))
    (netfarm-scripts:run-script-machines node)
    node))

(defun make-graph (&key (depth 16))
  "Generate a random graph that is up to DEPTH layers deep, and return one of the bottom-most nodes."
  (netfarm:with-hash-cache ()
    (let ((last-layer (list (make-graph-node))))
      (loop for layer below depth
            for this-layer = (loop for node in last-layer
                                   appending (loop repeat (if (< layer 3)
                                                              3
                                                              (binomial-random 10 0.15))
                                                   collect (make-graph-node node)))
            when (null this-layer)
              return nil
            do (setf last-layer this-layer))
      (alexandria:random-elt last-layer))))
