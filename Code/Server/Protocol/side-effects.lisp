(in-package :netfarm-server)

(defgeneric apply-side-effect (system cause side-effect-type &key)
  (:documentation "Apply a side effect caused by an object named CAUSE, of type SIDE-EFFECT-TYPE with some keyword arguments."))
(defgeneric objects-affected-by (system name)
  (:documentation "Returns a list of the names of every object that is affected by the object named NAME."))
(defgeneric objects-affecting (system name)
  (:documentation "Returns a list of the names of every object that affects the object named NAME."))

(defconstant +hash-byte-length+ (/ 256 8))
(defgeneric objects-affecting-hash (system name)
  (:documentation "Returns a hash value representing the set of the names of every object that affects the object named NAME.")
  (:method ((system netfarm-system) name)
    "This method computes the hash from the objects-affecting list. You can probably implement a much faster method."
    (flet ((base64->integer (string)
             (reduce (lambda (a v)
                       (logior (ash a 8) v))
                     (netfarm:base64->bytes string)
                     :initial-value 0))
           (*-mod-2^256 (a b)
             (ldb (byte 256 0) (* a b))))
      (reduce #'*-mod-2^256 (objects-affecting system name)
              :key #'base64->integer :initial-value 1))))

(defmethod apply-side-effect :around ((system netfarm-system) cause-name type &rest values &key target)
  (let ((target-name (netfarm:hash-object* target)))
    (decentralise-standard-system:with-consistent-block (cause-name system)
      (call-next-method)
      (broadcast-effect system target-name cause-name
                        (cons type values)))))

(defgeneric computed-value-counters (system name)
  (:documentation "Return a list of computed value counters for an object named NAME, like ((slot-position value count) ...)
Counters with a value of zero can be elided."))
