(in-package :netfarm-network-tests)

(defclass noisy-interactive (interactive plain)
  ())

(defun run-tests (&optional interactive?)
  (eql (status
	(test
         '(server-client-tests server-server-tests)
         :report (if interactive? 'interactive 'plain)))
       :passed))

(defun gitlab-ci-test ()
  (or (run-tests)
      (uiop:quit -1)))
