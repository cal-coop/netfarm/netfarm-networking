(in-package :netfarm-server)

(decentralise-standard-system:define-special-block
    (netfarm-system effect))

(defmethod decentralise-standard-system:add-block-information
    :around ((system netfarm-system) connection name version channels)
  "Handle broadcasts about the special block 'effect'."
  (if (string= name "effect")
      (destructuring-bind (|effect again| target cause) channels
        (declare (ignore |effect again|))
        (when (and (vague-object-stored-p system target)
                   (not (vague-object-stored-p system cause)))
          (add-other-interesting-block system cause)))
      (call-next-method)))

(defgeneric render-effect (type &key &allow-other-keys)
  (:method ((type (eql 'netfarm-scripts:add-computed-value))
            &key slot value target)
    (list "add-computed-value"
          (netfarm:slot-netfarm-name slot)
          value target))
  (:method ((type (eql 'netfarm-scripts:remove-computed-value))
            &key slot value target)
    (list "remove-computed-value"
          (netfarm:slot-netfarm-name slot)
          value target)))

(defun broadcast-effect (system target cause values)
  (decentralise-standard-system:broadcast-metadata system
                                                   "effect"
                                                   0
                                                   (list "effect" target cause))
  (with-unlocked-box (connections (decentralise-system:system-connections system))
    (dolist (connection connections)
      (when (decentralise-standard-system:connection-interested-in-p
             system connection "effect-data" 0 (list "effect-data" target))
        (ignore-errors
         (decentralise-connection:write-block
          connection "effect-data" 0 (list "effect-data" target)
          (decentralise-messages:object->data (apply #'render-effect values)
                                              connection
                                              'netfarm-decentralise2:netfarm-value)))))))
