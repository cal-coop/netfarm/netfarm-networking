(defpackage :netfarm-client
  (:use :cl :decentralise-utilities)
  (:export #:client #:save-object
           #:find-object #:save-single-object #:save-single-object*
           #:*find-object-arguments*
           #:object-requires-real-counters-p))
