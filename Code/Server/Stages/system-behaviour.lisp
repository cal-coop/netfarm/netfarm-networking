(in-package :netfarm-server)

(defmethod put-vague-object :before (system vague-object)
  (assert (not (null (netfarm:vague-object-name vague-object)))))

(defmethod get-vague-object :before (system name)
  (declare (ignore system))
  (assert (stringp name)))

(defmethod get-vague-object :around (system name)
  (multiple-value-bind (vague-object size)
      (call-next-method)
    (setf (netfarm:vague-object-source vague-object)
          (system-source system))
    (if (null size)
        vague-object
        (values vague-object size))))

(defmethod count-vague-objects ((system netfarm-system))
  (let ((count 0))
    (map-vague-objects (lambda (name)
                         (declare (ignore name))
                         (incf count))
                       system)
    count))

(defmethod count-presentable-objects ((system netfarm-system))
  (let ((count 0))
    (map-vague-objects (lambda (name)
                         (when (presentable-name-p system name)
                           (incf count)))
                       system)
    count))

(defun get-class (system name)
  (or (netfarm:find-netfarm-class name nil)
      (let* ((vague-object (get-vague-object system name))
             (schema (netfarm:apply-class vague-object
                                          (find-class 'netfarm:schema))))
        (netfarm:schema->class schema))))
(defun get-object (system name)
  (let ((object (netfarm:find-inbuilt-object name)))
    (unless (null object)
      (return-from get-object object)))
  (multiple-value-bind (object present?)
      (decentralise-utilities:with-unlocked-box
          (table (netfarm-system-cached-objects system))
        (gethash name table))
    (if present?
        object
        (let* ((vague-object (get-vague-object system name))
               (schema-name (netfarm:vague-object-schema-name vague-object))
               (class (get-class system schema-name))
               (object (netfarm:apply-class vague-object class)))
          (decentralise-utilities:with-unlocked-box
              (table (netfarm-system-cached-objects system))
            (setf (gethash object table) name))
          (decentralise-utilities:with-unlocked-box
              (table (netfarm-system-cached-objects system))
            (setf (gethash name table) object))
          object))))
(defun object-hash (system object)
  (decentralise-utilities:with-unlocked-box
      (table (netfarm-system-cached-objects system))
    (values (gethash object table))))

(defun system-source (system)
  (labels ((source (name)
             (handler-case
                 (get-object system name)
               (vague-object-not-found ()
                 (netfarm:object-not-found name #'source)))))
    #'source))

(defmethod vague-object-stored-p :around ((system netfarm-system) name)
  "A vague object corresponding to an inbuilt class or object is \"stored\" in any system."
  (or (and (netfarm:find-netfarm-class name nil) t)
      (and (netfarm:find-inbuilt-object name) t)
      (call-next-method)))

(defmethod (setf presentable-name-p) :after (presentable? (system netfarm-system) name)
  (when presentable?
    (decentralise-standard-system:broadcast-metadata system name 0 '("object"))))

(defun mark-as-presentable (system name)
  "Another way to write (SETF (PRESENTABLE-NAME-P SYSTEM NAME) T)"
  (setf (presentable-name-p system name) t))

(defmethod add-other-interesting-block :after ((system netfarm-system) name)
  (decentralise-standard-system:update-system-for-new-interesting-block-predicate
   system (list name) '())
  (map-connections-storing-object
   (lambda (connection)
     (decentralise-standard-system:add-block-information
      system connection name 0 '("object")))
   system name))
