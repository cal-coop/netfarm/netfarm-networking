(in-package :netfarm-client)

(defclass client (decentralise-kademlia:kademlia-client
                  netfarm-kademlia:netfarm-kademlia-format-mixin)
  ((subclient-table :initform (trivial-garbage:make-weak-hash-table
                               :test #'eq
                               :weakness :key)
                    :reader client-subclient-table
                    :documentation "A table mapping (weak) objects to the subclients we expect to get updates from.")
   (instance-table :initform (box (trivial-garbage:make-weak-hash-table
                                   :test #'equal
                                   :weakness-matters t
                                   :weakness :value))
                   :reader client-instance-table
                   :documentation "A table mapping names to (weak) objects that we need to update.")
   (counter-request-mailboxes :initform (box (make-hash-table :test #'equal))
                              :reader client-counter-request-mailboxes)
   (searching-instances :initform (box (trivial-garbage:make-weak-hash-table
                                        :test #'equal
                                        :weakness-matters t
                                        :weakness :value))
                        :reader client-searching-instances-table
                        :documentation "A table mapping names to (weak) object that we should subscribe to, but lost a connection for."))
  (:default-initargs :channels '("object")))

(defun find-retrieved-object (client hash)
  (with-unlocked-box (table (client-instance-table client))
    (multiple-value-bind (object present?)
        (gethash hash table)
      (if present?
          (values object
                  (gethash object (client-subclient-table client)))
          (values nil nil)))))

(defun %set-find-retrieved-object (table client hash object subclient)
  (setf (gethash hash table) object
        (gethash object (client-subclient-table client)) subclient))

(defun set-find-retrieved-object (client hash object subclient)
  (with-unlocked-box (table (client-instance-table client))
    (%set-find-retrieved-object table client hash object subclient)))

(defvar *find-object-arguments* '()
  "Additional arguments to pass FIND-OBJECT when retrieving objects for Netfarm.
For example, (:timeout 5) sets a timeout of 5 seconds, and (:follow-updates? t :timeout 10) sets a timeout of 10 seconds, and watches for updates.")

(defun get-vague-object* (client name &key follow-updates?)
  "Produce an action that returns a vague object.
If FOLLOW-UPDATES? is true, then we subscribe to changes on that object, and do the necessary bookkeeping to ensure we can put updates somewhere."
  (decentralise-client:do-subclients (subclient client `(:get ,name))
    (let ((we-added-subscription? nil))
      (when follow-updates?
        (unless (member name (decentralise-client:client-subscriptions subclient)
                        :test #'string=)
          (setf we-added-subscription? t)
          (push name (decentralise-client:client-subscriptions subclient))))
      (decentralise-client:add-handler
       subclient `(:block ,name)
       (lambda (message)
         (decentralise-messages:message-case message
           ((:block ~ ~ ~ data)
            (handler-case
                (decentralise-messages:data->object
                 data
                 (decentralise-client:client-connection subclient)
                 'netfarm:vague-object)
              (:no-error (vague-object)
                (when (netfarm:verify-object-hash vague-object
                                                  (netfarm:base64->bytes name))
                  ;; We should put any updates we receive (before applying a
                  ;; class to make a real object) on this vague object, and we
                  ;; will apply them when we receive a real object.
                  (set-find-retrieved-object client subclient
                                             vague-object subclient)
                  (setf (netfarm:vague-object-source vague-object)
                        (lambda (name)
                          (apply #'find-object client name *find-object-arguments*)))
                  (decentralise-client:succeed vague-object))))))))
      (decentralise-client:add-handler
       subclient `(:error ,name)
       (lambda (message)
         (declare (ignore message))
         (when we-added-subscription?
           (alexandria:removef (decentralise-client:client-subscriptions subclient)
                               name :test #'string=))
         (decentralise-client:next-subclient)))
      (decentralise-client:send-message
       subclient
       (decentralise-messages:message :get (list name))))))

(defmethod decentralise-kademlia:add-subclient-information :after
    ((client client) uri subclient)
  (let ((connection (decentralise-client:client-connection subclient)))
    (decentralise-client:with-consistent-state (subclient)
      (labels ((add-subclient-handler (name callback)
                 (decentralise-client:add-handler
                  subclient `(:block ,name)
                  (lambda (message)
                    (funcall callback message)
                    (add-subclient-handler name callback))))
               (message-handler (message)
                 (run-effect client subclient
                             message connection)))
        (with-unlocked-box (searching-table (client-searching-instances-table client))
          (maphash (lambda (hash instance)
                     (declare (ignore instance))
                     (decentralise-client:run-away
                      (get-real-counters* subclient hash)))
                   searching-table))
        (add-subclient-handler "counters"    #'message-handler)
        (add-subclient-handler "effect-data" #'message-handler)))))

(defmethod decentralise-kademlia:remove-subclient :after
    ((client client) subclient)
  (when (decentralise-client:client-running-p client)
    (with-unlocked-box (instance-table (client-instance-table client))
      (with-unlocked-box (searching-table (client-searching-instances-table client))
        (dolist (subscription (decentralise-client:client-subscriptions subclient))
          (let ((instance (gethash subscription instance-table)))
            (unless (null instance)
              ;; Remove the assumption that we should get updates from
              ;; this subclient; it's gone now. The first subclient we resync
              ;; with will replace it.
              (remhash instance (client-subclient-table client))
              (resubscribe-to client subscription instance searching-table)
              (warn "We were getting updates on ~s from ~s, but it disconnected"
                    instance subclient))))))))
