(in-package :netfarm-server)

(defclass tracing-mixin ()
  ()
  (:documentation "A mixin that \"traces\" the usage of values given to MAP- functions, in order to predict what should be prefetched. This could be used, say, to make more elaborate and efficient queries to a relational database, instead of sending many simpler and inefficient queries.")) 

;; Fallback methods for non-tracing methods.
(defgeneric map-vague-objects/named (name function system)
  (:method (name function system)
    (declare (ignore name))
    (let ((%%system-no-loop-pls system))
      (map-vague-objects function %%system-no-loop-pls))))

;; The compiler macro is evaluated exactly once per compiled form on these
;; platforms.
#+(or sbcl clozure clisp)
(progn
  (define-compiler-macro map-vague-objects (&whole w function system)
    (if (eql '%%system-no-loop-pls system)
        w
        `(map-vague-objects/named ',(gensym "MAP-VAGUE-OBJECTS")
                                  ,function ,system))))
