(in-package :netfarm-server)

(defvar *dump-readtable* (copy-readtable))
(defvar *reading-system* nil
  "The Netfarm system the interpreter runs in, or NIL")

(defun read-object (stream character number)
  (declare (ignore number character))
  (let ((name (read stream)))
    (check-type name string)
    (let* ((vague-object (get-vague-object *reading-system* name))
           (schema-name (netfarm:vague-object-schema-name vague-object))
           (schema (get-vague-object *reading-system* schema-name))
           (class (netfarm:schema->class schema)))
      (netfarm:apply-class vague-object class))))

(set-dispatch-macro-character #\# #\@ #'read-object *dump-readtable*)

(defmethod print-object :around ((object netfarm:object) stream)
  "Use the #@ syntax to print an object if we are dumping a machine."
  (if (null *reading-system*)
      (call-next-method)
      (let ((name (netfarm:object-name object)))
        (assert (not (null name))
                ()
                "Cannot dump an unnamed Netfarm object")
        (format stream "#@~s" name))))

(defun dump-interpreter-to-string (interpreter system)
  (let ((*reading-system* system)
        (*print-circle* t))
    (prin1-to-string interpreter)))
(defun read-interpreter-from-string (string system)
  (let ((*reading-system* system)
        (*readtable* *dump-readtable*))
    (read-from-string string)))
