(defpackage :netfarm-decentralise2
  (:use :cl)
  (:export #:netfarm-translator-mixin #:netfarm-value))
(in-package :netfarm-decentralise2)

;;; Render objects

(defmethod decentralise-messages:object->data
  ((object netfarm:vague-object) (c decentralise-connection:character-connection)
   (n (eql 'netfarm:vague-object)))
  (netfarm:render-object object))
(defmethod decentralise-messages:object->data
  ((object netfarm:vague-object) (c decentralise-connection:binary-connection)
   (n (eql 'netfarm:vague-object)))
  (netfarm:binary-render-object object))

(defmethod decentralise-messages:object->data
  ((object netfarm:object) (c decentralise-connection:character-connection)
   (n (eql 'netfarm:object)))
  (netfarm:render-object object))
(defmethod decentralise-messages:object->data
  ((object netfarm:object) (c decentralise-connection:binary-connection)
   (n (eql 'netfarm:object)))
  (netfarm:binary-render-object object))

;;; Parse vague objects

(defmethod decentralise-messages:data->object
  ((string string) (c decentralise-connection:character-connection)
   (target (eql 'netfarm:vague-object)))
  (netfarm:parse-block string))
(defmethod decentralise-messages:data->object
  ((bytes vector) (c decentralise-connection:binary-connection)
   (target (eql 'netfarm:vague-object)))
  (netfarm:binary-parse-block bytes))

;;; Netfarm values

(defmethod decentralise-messages:data->object
  ((string string) (c decentralise-connection:character-connection)
   (target (eql 'netfarm-value)))
  (netfarm:parse string))
(defmethod decentralise-messages:data->object
  ((bytes vector) (c decentralise-connection:binary-connection)
   (target (eql 'netfarm-value)))
  (netfarm:binary-parse bytes))

(defmethod decentralise-messages:object->data
    (object (c decentralise-connection:character-connection)
     (source (eql 'netfarm-value)))
  (netfarm:render object))
(defmethod decentralise-messages:object->data
    (object (c decentralise-connection:binary-connection)
     (source (eql 'netfarm-value)))
  (netfarm:binary-render object))
