(in-package :netfarm-client)

(defgeneric object-requires-real-counters-p (object)
  (:documentation "Should we fetch the real counter values for an object, so that the usual technique for incremental updates can work?
Usually this should be the case, but some specialised algorithms do not require the real counter values.")
  (:method ((object netfarm:object)) t))

(defun get-parsed-block-data (client name &key timeout follow-updates?)
  (destructuring-bind (vague-object subclient)
      (decentralise-client:run (decentralise-client:then
                                (get-vague-object* client name
                                             :follow-updates? follow-updates?)
                                (lambda (vague-object)
                                  (setf (netfarm:vague-object-source vague-object)
                                        (lambda (name)
                                          (find-object client name)))
                                  (decentralise-client:finish vague-object)))
                               :timeout timeout)
    (values vague-object subclient)))

(defun find-netfarm-class* (client name)
  (let ((inbuilt-class (netfarm:find-netfarm-class name nil)))
    (if (null inbuilt-class)
        (decentralise-client:then (get-vague-object* client name)
                                  (lambda (vague-object)
                                    (setf (netfarm:vague-object-source vague-object)
                                          (lambda (name)
                                            (find-object client name)))
                                    (decentralise-client:finish
                                     (netfarm:schema->class
                                      (netfarm:apply-class vague-object
                                                           (find-class 'netfarm:schema))))))
        (decentralise-client:finish inbuilt-class))))

(defun find-netfarm-class (client name &key timeout)
  (decentralise-client:run (find-netfarm-class* client name)
                           :timeout timeout))

(defun get-real-counters* (subclient name)
  (decentralise-client:functional-action
   (lambda (succeed fail)
     (declare (ignore fail))
     (decentralise-client:send-message
      subclient
      (decentralise-messages:message
       :block "request-counters"
       0 '()
       (decentralise-messages:object->data name
                                           (decentralise-client:client-connection subclient)
                                           'netfarm-decentralise2:netfarm-value)))
     (funcall succeed nil))))

(defun when-monad (test monad)
  (if test
      ;; Splice an action before the callback if the test is true.
      monad
      ;; Don't splice anything if the test is false.
      (decentralise-client:finish nil)))

(defun find-object* (client name &key follow-updates?)
  (decentralise-client:chain
    (vague-object <- (get-vague-object* client name
                                  :follow-updates? follow-updates?))
    (class <- (find-netfarm-class* client
                                   (netfarm:vague-object-schema-name vague-object)))
    (object = (netfarm:apply-class vague-object class))
    (subclient = (with-unlocked-box (instance-table (client-instance-table client))
                   (gethash vague-object (client-subclient-table client))))
    (~ <- (when-monad (object-requires-real-counters-p object)
                      (get-real-counters* subclient name)))
    (progn
      (when follow-updates?
        (with-unlocked-box (instance-table (client-instance-table client))
          (let ((subclient (gethash vague-object (client-subclient-table client))))
            (%set-find-retrieved-object instance-table client name object subclient)
            (with-unlocked-box (table *vague-object-effect-log-table*)
              ;; We PUSH effects onto the start of a list, and setting the
              ;; computed value counters of an object is not a commutative
              ;; operation.
              (netfarm-scripts:apply-side-effects-on
               object
               (reverse (gethash vague-object table '())))
              (remhash vague-object table)))))
      (decentralise-client:finish object))))

(defun find-object (client name &key timeout follow-updates?)
  (decentralise-client:run (find-object* client name
                                         :follow-updates? follow-updates?)
                           :timeout timeout))

(defvar *message-queue* (safe-queue:make-mailbox))
(defun message-queue-worker ()
  (loop for (message . failure) = (safe-queue:mailbox-receive-message *message-queue*)
        do (handler-case
               (funcall message)
             (error (e) (funcall failure e)))))

(dotimes (i 4)
  (bt:make-thread #'message-queue-worker
                  :name "Message serialization worker thread"))

(defun put-object* (client name object &key follow-updates?)
  (when (decentralise-client:client-has-block-p client name)
    (return-from put-object* (decentralise-client:finish nil)))
  (netfarm:with-lexical-hash-cache ()
    (decentralise-client:do-subclients (subclient client `(:block ,name))
      (let ((we-added-subscription? nil))
        (decentralise-client:add-handler
         subclient `(:ok ,name)
         (lambda (message)
           (declare (ignore message))
           (when follow-updates?
             (set-find-retrieved-object client name object subclient))
           (decentralise-client:succeed t)))
        (decentralise-client:add-handler
         subclient `(:error ,name)
         (lambda (message)
           ;; If the object has already been put, we could still listen for
           ;; effects on this client.
           (cond
             ((string= (decentralise-messages:error-response-reason message)
                       "too old")
              (decentralise-client:succeed t))
             (t
              (when we-added-subscription?
                (alexandria:removef (decentralise-client:client-subscriptions subclient)
                                    name :test #'string=))
              (decentralise-client:fail
                  (decentralise-client:condition-from-message subclient message))))))
        (when follow-updates?
          (unless (member name (decentralise-client:client-subscriptions subclient)
                          :test #'string=)
            (setf we-added-subscription? t)
            (push name (decentralise-client:client-subscriptions subclient))))
        (safe-queue:mailbox-send-message
         *message-queue*
         (cons
          (lambda ()
            (netfarm:with-restored-lexical-hash-cache ()
              (let* ((connection (decentralise-client:client-connection subclient))
                     (data (decentralise-messages:object->data object connection 'netfarm:object)))
                (decentralise-client:send-message
                 subclient
                 (decentralise-messages:message :block name 0 '("object") data)))))
          (lambda (e) (decentralise-client:fail e))))))))

(defun save-single-object* (client object &key follow-updates?)
  (let ((name (netfarm:hash-object* object)))
    (put-object* client name object
                 :follow-updates? follow-updates?)))

(defun save-single-object (client object &key timeout follow-updates?)
  (let ((request (save-single-object* client object
                                      :follow-updates? follow-updates?)))
    (unless (null request)
      (decentralise-client:run request :timeout timeout))))

(defun save-object (client object &key timeout follow-updates?)
  (netfarm:with-hash-cache ()
    (let ((requests '()))
      (netfarm:do-objects (subobject object)
        (unless (string= "inbuilt@" (netfarm:hash-object* object) :end2 8)
          (push (save-single-object* client subobject
                                     :follow-updates? (and (eq object subobject)
                                                           follow-updates?))
                requests)))
      (decentralise-client:run (decentralise-client:parallel requests)
                               :timeout timeout))
    (netfarm:hash-object* object)))
