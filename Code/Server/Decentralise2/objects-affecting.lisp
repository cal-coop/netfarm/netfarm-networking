(in-package :netfarm-server)

(decentralise-standard-system:define-special-block
    (netfarm-system request-objects-affecting netfarm-decentralise2:netfarm-value)
  :put (lambda (system connection version channels name)
         (declare (ignore version channels))
         (check-type name string)
         (let* ((affected-by (objects-affecting system name))
                (data
                  (decentralise-messages:object->data (cons name affected-by)
                                                      connection
                                                      'netfarm-decentralise2:netfarm-value)))
           (decentralise-connection:write-block connection
                                                "objects-affecting"
                                                0
                                                '()
                                                data))))

(decentralise-standard-system:define-special-block
    (netfarm-system objects-affecting netfarm-decentralise2:netfarm-value)
  :put (lambda (system connection version channels object-names)
         (declare (ignore version channels))
         (destructuring-bind (name &rest object-names)
             object-names
           (when (vague-object-stored-p system name)
             (dolist (object-name object-names)
               (unless (vague-object-stored-p system object-name) 
                 (add-other-interesting-block system object-name)
                 (decentralise-standard-system:add-block-information
                  system connection object-name 0 '("object"))))))))

(defun request-objects-affecting (connection name)
  (let ((rendered-name
          (decentralise-messages:object->data name
                                              connection
                                              'netfarm-decentralise2:netfarm-value)))
    (decentralise-connection:write-block connection
                                         "request-objects-affecting"
                                         0 '()
                                         rendered-name)))

(defmethod decentralise-standard-system:add-block-information :after
    ((system netfarm-system) connection name version channels)
  (declare (ignore version channels))
  (when (vague-object-stored-p system name)
    (request-objects-affecting connection name))
  (add-to-connection-bloom-filter system connection name))
