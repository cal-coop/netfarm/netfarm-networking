(asdf:defsystem :netfarm-client
  :author "The Cooperative of Applied Language"
  :description "A system that extends the decentralise2 client to speak Netfarm"
  :depends-on (:decentralise2 :netfarm :netfarm-decentralise2)
  :components ((:file "package")
               (:file "client"  :depends-on ("package"))
               (:file "object"  :depends-on ("package"))
               (:file "updates" :depends-on ("client"))))
