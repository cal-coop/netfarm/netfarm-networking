(in-package :netfarm-server)

(defmethod decentralise-messages:object->data
    (object
     (target decentralise-connection:binary-connection)
     (source (eql 'netfarm-value)))
  (netfarm:binary-render object))
(defmethod decentralise-messages:object->data
    (object
     (target decentralise-connection:character-connection)
     (source (eql 'netfarm-value)))
  (netfarm:render object))

(defmethod decentralise-messages:data->object
    (data
     (source decentralise-connection:binary-connection)
     (target (eql 'netfarm-value)))
  (netfarm:binary-parse data))
(defmethod decentralise-messages:data->object
    (data
     (source decentralise-connection:character-connection)
     (target (eql 'netfarm-value)))
  (netfarm:parse data))
