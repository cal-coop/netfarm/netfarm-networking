(in-package :netfarm-server-stress-test)

(defmacro parallel (&body body)
  `(mapc #'bt:join-thread
         (list ,@(loop for form in body
                       collect `(bt:make-thread (lambda () ,form))))))

(defmacro with-timing ((name) &body body)
  (alexandria:with-gensyms (start-time)
    `(let ((,start-time (get-internal-real-time)))
       (multiple-value-prog1
           (progn ,@body)
         (format *debug-io* "~a took ~3f seconds"
                 ',name
                 (/ (- (get-internal-real-time) ,start-time)
                    internal-time-units-per-second))))))

(defun wait-for-system-size (system size &key (timeout 20) (period 0.1))
  (loop for time from 0 to timeout by period
        for presentable-size = (netfarm-server:count-presentable-objects system)
        do (format *debug-io* "~&At ~,1f seconds, the system has ~d presentable object~:p~%"
                   time presentable-size)
           (sleep period)
        if (>= presentable-size size)
          return t
        finally (error "waited too long for system to store ~d presentable object~:p"
                       presentable-size)))

(defun make-test-client (system)
  (multiple-value-bind (connection1 connection2)
      (decentralise-connection:make-hidden-socket)
    (decentralise-system:add-connection system connection1)
    (make-instance 'netfarm-client:client
                   :bootstrap-connections (list connection2))))
                 
(defun %stress-test-system (system memory-system)
  (declare (ignore system))
  (let* ((memory-client (make-test-client memory-system))
         (graph (make-graph))
         (graph-size (graph-size (parent* graph)))
         (requests (make-array 20000 :fill-pointer 0 :adjustable t)))
    (netfarm-client:save-object memory-client
                                (class->schema (find-class 'graph)))
    (netfarm:with-hash-cache ()
      (map-nodes (lambda (node)
                   (vector-push-extend (netfarm-client:save-single-object* memory-client node)
                                       requests))
                 (parent* graph)))
    (format *debug-io* "Saving ~d objects..." graph-size)
    (let ((start-time (get-internal-real-time)))
      (parallel
        (wait-for-system-size memory-system graph-size)
        (decentralise-client:run (decentralise-client:parallel requests)))
      (decentralise-client:stop-client memory-client)
      (values graph-size memory-system
              (/ (- (get-internal-real-time) start-time)
                 internal-time-units-per-second)))))

(defun stress-test-system (system
                           &key (memory-system
                                 (make-instance 'netfarm-server:memory-system)))
  (decentralise-utilities:reset-meters '())
  (decentralise-system:start-system memory-system)
  #+(or)
  (decentralise-system:start-system system)
  (unwind-protect
       (%stress-test-system system memory-system)
    #+(or)
    (decentralise-system:stop-system system)
    (decentralise-system:stop-system memory-system)
    (decentralise-utilities:print-meters '())))

(defun benchmark-table (&key (repetitions 100))
  (loop repeat repetitions
        for (graph-size nil time)
          = (multiple-value-list (stress-test-system nil))
        collect (list graph-size time)))

(defun print-benchmark-table (&rest rest &key repetitions)
  (declare (ignore repetitions))
  (format t "~{~&~{~d, ~f~}~}"
          (sort (apply #'benchmark-table rest) #'< :key #'first)))
