(asdf:defsystem :netfarm-decentralise2
  :depends-on (:decentralise2)
  :components ((:file "kademlia")
               (:file "mixin")))
