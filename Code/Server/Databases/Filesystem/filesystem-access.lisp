(in-package :netfarm-server-filesystem-database)

(defmacro with-locked-file ((stream system name &rest options) &body body)
  (alexandria:once-only (name)
    `(with-named-resource (,name (filesystem-database-access-table ,system))
       (with-open-file (,stream ,name ,@options)
         ,@body))))

(defconstant +characters-per-level+ 2)
(defconstant +levels+ 4)
(defun split-object-hash (object-hash)
  (values (loop for level below +levels+
                for start = (* +characters-per-level+ level)
                for end   = (+ start +characters-per-level+)
                collect (subseq object-hash start end))
          (subseq object-hash (* +characters-per-level+ +levels+))))

(defgeneric system-relative-pathname (system pathname)
  (:method ((system filesystem-database-mixin) pathname)
    (merge-pathnames pathname (filesystem-database-directory system))))

(defgeneric object-pathname (system object-hash type)
  (:method ((system filesystem-database-mixin) object-hash type)
    (setf object-hash (substitute #\- #\/ object-hash)) 
    (multiple-value-bind (directories name)
        (split-object-hash object-hash)
      (system-relative-pathname system
                                (make-pathname :directory (cons :relative directories)
                                               :name name
                                               :type (string-downcase type))))))


(defun invoke-until-empty (stream function)
  (loop with length = (file-length stream)
        until (= (file-position stream) length)
        do (funcall function)))

(defmacro do-until-empty ((stream) &body body)
  `(invoke-until-empty ,stream (lambda () ,@body)))
