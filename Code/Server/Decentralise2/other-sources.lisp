(in-package :netfarm-server)

(defmethod decentralise-system:connect-system :after
    ((system netfarm-system) (connection decentralise-connection:connection))
  (setf (decentralise-system:connection-value system connection 'storing)
        ;; Set these values to your liking - but this should be pretty reasonable.
        (box (cl-bloom:make-filter :capacity 10000 :false-drop-rate 0.001))))

(defun connection-has-object-p (system connection name)
  "Does the connection (probably) have an object?"
  (let ((filter (decentralise-system:connection-value system connection
                                                      'storing)))
    (if (null filter)
        nil
        (with-unlocked-box (filter filter)
          (cl-bloom:memberp filter name)))))

(defun add-to-connection-bloom-filter (system connection name)
  "Remember that a connection has an object."
  (let ((filter (decentralise-system:connection-value system connection
                                                      'storing)))
    (unless (null filter)
      (with-unlocked-box (filter filter)
        (cl-bloom:add filter name)))))

(defun map-connections-storing-object (function system name)
  (with-unlocked-box (connections (decentralise-system:system-connections system))
    (dolist (connection connections)
      (when (connection-has-object-p system connection name)
        (funcall function connection)))))

(defmethod put-vague-object :after ((system netfarm-system) vague-object)
  (let ((name (netfarm:hash-object* vague-object)))
    (map-connections-storing-object (lambda (connection)
                                      (request-objects-affecting connection name))
                                    system name)))
