(in-package :netfarm-server)

(defstruct interpreter-state
  interpreter name)

(defun mark-script-machine-dependency (system name condition)
  ;; The script machine tried to load an object we don't have, so mark it
  ;; as a dependency and try again later.
  (add-dependency-edge system
                       :script-machine-load
                       name
                       (netfarm:object-not-found-name condition)))

(define-time-meter *run-script-machine*
  "Running script machines" "netfarm-server")
(defgeneric run-script-machine (system)
  (:method ((system netfarm-system))
    (with-received-message
        (state (netfarm-system-interpreters system))
      (let ((interpreter (interpreter-state-interpreter state)))
        (handler-case
            (with-time-meter (*run-script-machine*)
              (netfarm-scripts:run-interpreter
               interpreter
               :cycle-limit netfarm-scripts:*script-instruction-limit*))
          (netfarm:object-not-found (o)
            (mark-script-machine-dependency system
                                            (interpreter-state-name state)
                                            o))
          (:no-error (stack finished-interpreter)
            (declare (ignore finished-interpreter stack))
            (interpreter-state-completed state system))
          (error (e)
            (print e)))))))

(defgeneric add-initialization (system name object)
  (:method ((system netfarm-system) name object)
    (handler-case
        (netfarm-scripts:setup-method-interpreter object
                                                  "initialize" '()
                                                  '(:write-computed-values))
      (:no-error (interpreter)
        (safe-queue:mailbox-send-message (netfarm-system-interpreters system)
                                         (make-interpreter-state
                                          :interpreter interpreter
                                          :name name)))
      (netfarm:object-not-found (o)
        (mark-script-machine-dependency system name o))
      (error ()
        (mark-as-presentable system name)))))

(define-time-meter *apply-side-effects*
  "Applying side effects" "netfarm-server stages")

(defgeneric interpreter-state-completed (state system)
  (:method (state (system netfarm-system))
    (mark-as-presentable system (interpreter-state-name state))
    (let ((side-effects
            (netfarm-scripts:interpreter-side-effects
             (interpreter-state-interpreter state))))
      (dolist (effect side-effects)
        (with-time-meter (*apply-side-effects*)
          (apply #'apply-side-effect system
                 (interpreter-state-name state)
                 effect))))))

(defmethod handle-fulfilled-dependencies ((system netfarm-system)
                                          (type (eql ':script-machine-load))
                                          dependent)
  (add-initialization system dependent (get-object system dependent)))
