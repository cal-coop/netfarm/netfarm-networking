(in-package :netfarm-network-tests)

(define-test server-server-tests)

(defclass test-system (netfarm-server:memory-system)
  ((name :initarg :name :initform "I dunno" :reader test-system-name)))
(defmethod print-object ((system test-system) stream)
  (print-unreadable-object (system stream :type t :identity t)
    (write-string (test-system-name system) stream)))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun make-server-bindings (server-specs)
    (loop for (variable connected-to . plist) in server-specs
          collect `(,variable (make-instance 'test-system
                                             :name ,(symbol-name variable)
                                             ,@plist))))

  (defun make-inter-server-connections (server-specs)
    (loop for (variable connected-to . plist) in server-specs
          appending (loop for server in connected-to
                          for a = (gensym "A")
                          for b = (gensym "B")
                          collect `(multiple-value-bind (,a ,b)
                                       (decentralise-connection:make-hidden-socket)
                                     (decentralise-system:add-connection
                                      ,server ,a)
                                     (decentralise-system:connect-system
                                      ,variable ,b)))))
  
  (defun make-client-bindings (client-specs)
    (loop for (variable server) in client-specs
          for a = (gensym "A")
          for b = (gensym "B")
          collect `(,variable (multiple-value-bind (,a ,b)
                                  (decentralise-connection:make-hidden-socket)
                                (decentralise-system:add-connection
                                 ,server ,a)
                                (make-instance 'netfarm-client:client
                                               :bootstrap-connections (list ,b)))))))

(defmacro with-servers ((&rest servers) (&rest clients) &body body)
  `(let ,(make-server-bindings servers)
     (declare (special ,@(mapcar #'first servers)))
     (let ,(make-client-bindings clients)
       (declare (special ,@(mapcar #'first clients)))
       (unwind-protect
            ,@(loop for (name) in servers
                    collect `(decentralise-system:start-system ,name))
         ,@(make-inter-server-connections servers)
         ,@body)
       ,@(loop for (name) in servers
               collect `(decentralise-system:stop-system ,name))
       ,@(loop for (name) in clients
               collect `(decentralise-client:stop-client ,name)))
     (values ,@(mapcar #'first servers))))

(define-test simple-synchronisation
  :parent server-server-tests
  (with-servers ((*a* ())
                 (*b* (*a*)))
      ((*a-client* *a*)
       (*b-client* *b*))
    (let* ((object (make-instance 'a
                                  :a "This is the value for the slot A"))
           (hash   (netfarm:hash-object* object)))
      (netfarm-client:save-object *a-client* object)
      (wait-for-server-to-catch-up *b* *b-client* :minimum-presentable-count 1)
      (let ((object* (netfarm-client:find-object *b-client* hash)))
        (is string= "This is the value for the slot A" (a object*))))))

;; We no longer consider references to be dependencies, as long as a script
;; machine doesn't try to access a slot containing the reference.
#+(or)
(define-test synchronisation-that-has-to-find-dependencies
  :parent server-server-tests
  ;; Create two servers at opposite ends of hash space.
  (with-servers ((*a* (*b*) :id 0                 :distance 255)
                 (*b* ()    :id (1- (expt 2 256)) :distance 255))
      ((*a-client* *a*)
       (*b-client* *b*))
    (let* (;; This object will go on *B*, its hash starts with 160
           (b-object (make-instance 'a :a "Another object"))
           ;; This object will go on *A*, its hash starts with 93
           (a-object (make-instance 'a :a b-object
                                       :b "Every time you complain, we add another object")))
      (netfarm-client:save-object *b-client* b-object)
      ;; *A* should be able to find the dependent object, as it should be added
      ;; to its interesting set.
      (netfarm-client:save-single-object *a-client* a-object)
      (wait-for-server-to-catch-up)
      (let ((object* (netfarm-client:find-object *a-client*
                                                 (netfarm:hash-object* a-object))))
        (is string= "Every time you complain, we add another object" (b object*))
        (is string= "Another object" (a (a object*)))))))


(define-test synchronise-computed-value
  :parent server-server-tests
  (with-servers ((*a* (*b*) :id (1- (expt 2 256)) :distance 255)
                 (*b* (*a*) :id 0                 :distance 255))
    ((*a-client* *a*)
     (*b-client* *b*))
    (let* (;; This object will go on *B*, its hash starts with 93
           (b-object (make-instance 'c :text "One object"))
           ; This object will go on *A*, its hash starts with 223
           (a-object (make-instance 'b :my-c b-object)))
      (netfarm-client:save-object *a-client* a-object)
      (netfarm-client:save-object *b-client* b-object)
      (wait-for-server-to-catch-up *b* *b-client* :minimum-presentable-count 3)
      (let ((b-object*
              (netfarm-client:find-object *b-client*
                                          (netfarm:hash-object* b-object))))
        (is = 1 (length (your-b b-object*)))))))
