(in-package :netfarm-server)

(decentralise-standard-system:define-special-block
    (netfarm-system request-counters netfarm-decentralise2:netfarm-value)
  :put (lambda (system connection version channels name)
         (declare (ignore version channels))
         (check-type name string)
         (decentralise-standard-system:with-consistent-block (name system)
           (let* ((counters (computed-value-counters system name))
                  (data (decentralise-messages:object->data (cons name counters)
                                                            connection
                                                            'netfarm-decentralise2:netfarm-value)))
             (decentralise-connection:write-block connection
                                                  "counters"
                                                  0
                                                  '()
                                                  data)))))

(decentralise-standard-system:define-special-block
    (netfarm-system counters))
