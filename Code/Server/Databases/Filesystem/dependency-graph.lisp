(in-package :netfarm-server-filesystem-database)

;;; The dependency graph is stored in a file named `dependency-graph` in the
;;; system's storage directory.

;; A dependency graph is serialized as a stream of (DEPENDENT DEPENDENCY)
;; lists in the Netfarm format.

(defun read-dependency-graph (system stream &key (test (constantly 't)))
  "Read in a dependency graph, reading its contents from the stream. 
Edges are only added when the test is satisfied by the dependent and dependency of them."
  (flet ((reader ()
           (read-byte stream)))
    (do-until-empty (stream)
      (destructuring-bind (type-name dependent dependency)
          (netfarm:binary-parse #'reader)
        (let ((type (intern type-name :keyword)))
          (when (funcall test dependent dependency)
            (netfarm-server:add-dependency-edge system type dependent dependency)))))))

(defun write-dependency-graph (system stream)
  "Write the contents of a dependency graph to the stream."
  (netfarm-server:map-dependency-edges
   (lambda (type dependent dependency)
     (netfarm:binary-render (list (symbol-name type) dependent dependency)
                            (lambda (sequence)
                              (write-sequence sequence stream))))
   system))

(defun dependency-graph-pathname (system)
  (system-relative-pathname system #p"dependency-graph"))

(defgeneric load-dependency-graph (system)
  (:method ((system filesystem-database-mixin))
    (let ((dependency-graph-pathname
            (dependency-graph-pathname system)))
      (cond
        ((probe-file dependency-graph-pathname)
         (with-open-file (stream dependency-graph-pathname
                                 :direction :input
                                 :element-type '(unsigned-byte 8))
           (read-dependency-graph system stream)))
        (t
         (format *debug-io* "~&~s does not exist; creating a new dependency graph."
                 dependency-graph-pathname))))))

(defgeneric save-dependency-graph (system)
  (:method ((system filesystem-database-mixin))
    (with-open-file (stream (dependency-graph-pathname system)
                            :direction :output
                            :element-type '(unsigned-byte 8)
                            :if-exists :rename)
      (write-dependency-graph system stream))))

;; Writing out-of-sync dependency graphs is currently going to be very bad.
;; If there are dependencies in the graph that are fulfilled, they will appear
;; "stuck". Dependencies that should exist that don't will never be fulfilled.
#+(or)
(defun periodically-save-dependency-graph (system)
  (sleep 120)
  (save-dependency-graph system))
