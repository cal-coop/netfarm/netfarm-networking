(in-package :netfarm-server)

(decentralise-standard-system:define-simple-error incorrect-hash)

(define-time-meter *verifying-hash*
  "Verifying object hashes" "netfarm-server")
(define-time-meter *put-block*
  "Putting vague objects" "netfarm-server stages")

(defmethod decentralise-system:new-version-p
    ((system netfarm-system) name version)
  "There is only ever one 'version' of a Netfarm object."
  (not (vague-object-stored-p system name)))

(defmethod decentralise-system:put-block
    ((system netfarm-system) name version channels vague-object)
  (declare (ignore version channels))
  (with-time-meter (*put-block*)
    ;; At this point, we have already checked if we have stored this object
    ;; already. And it cannot be an inbuilt object, because then the hash has
    ;; no hopes of being correct.
    (setf (netfarm:vague-object-name vague-object) name
          (netfarm:vague-object-source vague-object) (system-source system))
    ;; Clear out computed values; we'll replicate them ourselves.
    (fill (netfarm:vague-object-computed-values vague-object) '())
    (if (with-time-meter (*verifying-hash*)
          (ignore-errors
           (netfarm:verify-object-hash vague-object
                                       (netfarm:base64->bytes name))))
        (add-new-vague-object system vague-object)
        (error 'incorrect-hash))))

(defmethod decentralise-system:get-block
    ((system netfarm-system) name)
  (if (and (vague-object-stored-p system name)
           (presentable-name-p system name))
      (handler-case (get-vague-object system name)
        (:no-error (object &rest stuff)
          (declare (ignore stuff))
          (values 0 '("object") object))
        (vague-object-not-found ()
          (error 'decentralise-system:not-found :name name)))
      (error 'decentralise-system:not-found :name name)))

(defmethod decentralise-system:get-block-metadata
    ((system netfarm-system) name)
  (if (and (vague-object-stored-p system name)
           (presentable-name-p system name))
      (values 0 '("object"))
      (error 'decentralise-system:not-found :name name)))

(defconstant +hash-byte-length+ (/ 256 8))
(defun integer->base64 (integer)
  (let ((buffer (make-array +hash-byte-length+
                            :element-type '(unsigned-byte 8))))
    (loop for position below +hash-byte-length+
          for buffer-position = (- +hash-byte-length+ position 1)
          until (zerop integer)
          do (setf (aref buffer buffer-position)
                   (ldb (byte 8 0) integer)
                   integer (ash integer -8)))
    (netfarm:bytes->base64 buffer)))
(defmethod decentralise-system:map-blocks
    (function (system netfarm-system))
  (map-vague-objects
   (lambda (name)
     (when (presentable-name-p system name)
       (funcall function name 0
                `("object"
                  "affecting-hash" ,(integer->base64
                                     (objects-affecting-hash system name))))))
   system))

(defmethod decentralise-standard-system:interesting-block-p
    ((system netfarm-system) name version channels)
  "An object is interesting if its hash is in our Kademlia space, or we need it for something else."
  (declare (ignore version channels))
  (or (call-next-method)
      (other-interesting-block-p system name)))

(defmethod decentralise-standard-system:uninteresting-block-p
    ((system netfarm-system) name version channels)
  nil)

(defmethod decentralise-system:start-system :after ((system netfarm-system))
  "Start auxiliary threads that resolve Netfarm objects."
  (netfarm:with-hash-cache ()
    (netfarm:with-lexical-hash-cache ()
      (setf (netfarm-system-worker-threads system)
            (loop for function in (compute-threads system)
                  collect (let ((function function))
                            (decentralise-utilities:with-thread
                                (:name (format nil "~s worker running ~a"
                                               (class-name (class-of system))
                                               function))
                              (netfarm:with-restored-lexical-hash-cache ()
                                (funcall-loop function system)))))))))

(defmethod decentralise-standard-system:distribute-after-put-block
    ((system netfarm-system) name version channels)
  "Only broadcast information about an object after it has gone through the resolution machinery." 
  (declare (ignore name version channels))
  '|You can't see it, 'til it's finished|)

(defmethod decentralise-standard-system:system-synchronisation-channels
    ((system netfarm-system))
  '("object" "effect"))
