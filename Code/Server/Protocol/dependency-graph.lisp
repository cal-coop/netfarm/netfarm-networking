(in-package :netfarm-server)

;;; Dependency graph storage protocol
(defgeneric add-dependency-edge (system type dependent dependency)
  (:documentation "Add a dependency edge to the dependency graph to the system.")
  (:method :after ((system netfarm-system) type dependent dependency)
    (declare (ignore dependent))
    ;; Sometimes we create an edge because we couldn't find an object, but
    ;; it is stored just before we create the edge; so check if it is stored now.
    (when (vague-object-stored-p system dependency)
      (remove-dependents system dependency)
      (return-from add-dependency-edge))
    (add-other-interesting-block system dependency)))

(decentralise-utilities:defexpectation add-dependency-edge
    (system (type keyword) (dependent string) (dependency string)))

(defun mark-as-fulfilled (system vague-object)
  (safe-queue:mailbox-send-message (netfarm-system-fulfilled-objects system)
                                   vague-object)
  t)


(defgeneric handle-fulfilled-dependencies (system type dependent)
  (:method ((system netfarm-system) (type (eql ':dependency)) dependent)
    (mark-as-fulfilled system (get-vague-object system dependent))))

(define-time-meter *removing-dependents*
  "Removing fulfilled dependencies" "netfarm-server")

(defgeneric remove-dependents (system dependency)
  (:documentation "Remove all dependency vertices from the dependency graph of the system where the dependency is DEPENDENCY, and return a list of (DEPENDENT TYPE) which were removed.")
  (:method :around ((system netfarm-system) dependency)
    (with-time-meter (*removing-dependents*)
      (let ((dependents (call-next-method)))
        (dolist (pair dependents)
          (destructuring-bind (dependent type) pair
            ;; Mark every dependent that has no more dependencies as fulfilled.
            (when (no-dependencies-p system type dependent)
              (handle-fulfilled-dependencies system type dependent))))
        dependents))))

(defgeneric map-dependents (function system type dependency)
  (:documentation "Repeatedly call FUNCTION with the names of objects that are dependent (because of the given dependency type) on the dependency."))
(decentralise-utilities:defexpectation map-dependents
    (function (system netfarm-system) (type keyword) (dependency string)))
(defgeneric map-dependencies (function system type dependent)
  (:documentation "Repeatedly call FUNCTION with the names of objects that are dependencies (because of the given dependency type) of the dependent."))
(defgeneric map-dependency-edges (function system)
  (:documentation "Repeatedly call FUNCTION with the names of all dependency types, dependents and dependencies."))

(defgeneric no-dependencies-p (system type name)
  (:documentation "Return true if the vague object named NAME has no dependencies.
A simple method is provided for this generic function, but it may be faster to provide your own method.")
  (:method ((system netfarm-system) type name)
    (map-dependencies (lambda (type dep)
                        (declare (ignore type dep))
                        (return-from no-dependencies-p nil))
                      system type name)
    t))
