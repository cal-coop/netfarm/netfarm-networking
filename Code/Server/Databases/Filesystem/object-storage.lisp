(in-package :netfarm-server-filesystem-database)

(defmacro with-new-binary-file ((stream system name type
                                 &key (make-directory? nil))
                                &body body)
  (alexandria:once-only (system)
    (alexandria:with-gensyms (pathname length)
      `(let ((,pathname (object-pathname ,system ,name ,type))
             (,length 0))
         ,@(if make-directory?
               `((ensure-directories-exist ,pathname))
               '())
         (with-open-file (,stream ,pathname
                                  :direction :output
                                  :element-type '(unsigned-byte 8)
                                  :if-exists :error)
           (symbol-macrolet ((writer (lambda (sequence)
                                       (incf ,length (length sequence))
                                       (write-sequence sequence ,stream))))
             ,@body)
           ,length)))))

(defmacro with-appending-binary-file ((stream system name type) &body body)
  (alexandria:once-only (system)
    (alexandria:with-gensyms (pathname)
      `(let ((,pathname (object-pathname ,system ,name ,type)))
         (with-open-file (,stream ,pathname
                                  :direction :output
                                  :element-type '(unsigned-byte 8)
                                  :if-exists :append
                                  :if-does-not-exist :error)
           (symbol-macrolet ((writer (lambda (sequence)
                                       (write-sequence sequence ,stream))))
             ,@body))))))

(defmacro with-open-binary-file ((stream system name type) &body body)
  (alexandria:once-only (system)
    (alexandria:with-gensyms (pathname)
      `(let ((,pathname (object-pathname ,system ,name ,type)))
         (with-open-file (,stream ,pathname
                                  :direction :input
                                  :element-type '(unsigned-byte 8)
                                  :if-does-not-exist :error)
           (symbol-macrolet ((reader (lambda ()
                                       (read-byte ,stream))))
             ,@body))))))

(defgeneric populate-vague-object-table (system)
  (:method ((system filesystem-database-mixin))
    (let ((table (filesystem-database-vague-objects system)))
      (labels ((recurse (pathname)
                 (cond
                   ((null (pathname-name pathname))
                    ;; This is a directory.
                    (mapc #'recurse
                          (directory
                           (make-pathname :directory (pathname-directory pathname)
                                          :name :wild
                                          :type :wild)))
                    nil)
                   ;; This is a file.
                   ((string= (pathname-type pathname) "object")
                    (setf (concurrent-hash-table:getchash
                           (pathname-name pathname)
                           table)
                          t)))))
        (recurse (filesystem-database-directory system))))))

(defgeneric add-vague-object (system name)
  (:method ((system filesystem-database-mixin) name)
    (setf (concurrent-hash-table:getchash
           name
           (filesystem-database-vague-objects system))
          t)))

(defmethod netfarm-server:map-vague-objects
    (function (system filesystem-database-mixin))
  (concurrent-hash-table:mapchash
   (lambda (name value)
     (declare (ignore value))
     (funcall function name))
   (filesystem-database-vague-objects system)))

(defgeneric %put-vague-object (system vague-object)
  (:method ((system filesystem-database-mixin) vague-object)
    (let ((name (netfarm:vague-object-name vague-object)))
      (with-named-resource (name (filesystem-database-access-table system))
        (let ((length
                (with-new-binary-file (s system name "object" :make-directory? t)
                  (netfarm:binary-render-object vague-object
                                                :function writer))))
          (add-vague-object system name)
          (with-new-binary-file (s system name "effects"))
          (with-new-binary-file (s system name "affected-by"))
          (with-new-binary-file (s system name "affects"))
          length)))))

(define-time-meter *put-vague-object*
    "Writing objects" "filesystem-database")
(defmethod netfarm-server:put-vague-object
    ((system filesystem-database-mixin) vague-object)
  (with-time-meter (*put-vague-object*)
    (%put-vague-object system vague-object)))

;;; Computed values are stored as (computed-slot-position cause value) lists
;;; appended to the end of the vague object.

(define-time-meter *get-vague-object*
  "Retrieving objects" "filesystem-database")

(defmethod netfarm-server:get-vague-object
    ((system filesystem-database-mixin) name)
  (with-named-resource (name (filesystem-database-access-table system))
    (with-time-meter (*get-vague-object*)
      (handler-case
          (%get-vague-object system name)
        (file-error ()
          (error 'netfarm-server:vague-object-not-found
                 :name name))))))

(defgeneric %get-vague-object (system name)
  (:method ((system filesystem-database-mixin) name)
    (let (vague-object
          computed-value-vector
          (count-table (make-hash-table :test 'equal))
          (read-length 0))
      (with-open-binary-file (s system name "object")
        (let ((position 0))
          (flet ((reader ()
                   (incf position)
                   (read-byte s)))
            (setf vague-object
                  (netfarm:binary-parse-block #'reader :name name)
                  computed-value-vector
                  (netfarm:vague-object-computed-values vague-object)
                  read-length position))))
      (with-open-binary-file (effects system name "effects")
        (do-until-empty (effects)
          (let ((position 0))
            (flet ((effect-reader ()
                     (incf position)
                     (read-byte effects)))
              (destructuring-bind (delta slot-position value)
                  (netfarm:binary-parse #'effect-reader)
                (let ((value (netfarm:normalise-graph value)))
                  (incf (gethash (list slot-position value)
                                 count-table 0)
                        delta))))
            (incf read-length position))))
      (loop for (slot-position value) being the hash-keys of count-table
            for count being the hash-values of count-table
            ;; Add computed values when the count is positive.
            when (plusp count)
              do (alexandria:appendf
                  (aref computed-value-vector slot-position)
                  (make-list count :initial-element value)))
      (values vague-object read-length))))

(defmethod netfarm-server:computed-value-counters
    ((system filesystem-database-mixin) name)
  (let ((count-table (make-hash-table :test 'equal)))
    (with-open-binary-file (effects system name "effects")
      (do-until-empty (effects)
        (destructuring-bind (delta slot-position value)
            (netfarm:binary-parse #'reader)
          (let ((value (netfarm:normalise-graph value)))
            (incf (gethash (list slot-position value)
                           count-table 0)
                  delta)))))
    (loop for (slot-position value) being the hash-keys of count-table
          for count being the hash-values of count-table
          unless (zerop count)
            collect (list slot-position value count))))

(defmethod netfarm-server:apply-side-effect
    ((system filesystem-database-mixin) cause-name
     (type (eql 'netfarm-scripts:add-computed-value))
     &key name value target)
  (let* ((target-name   (netfarm:hash-object* target))
         (vague-object  (netfarm-server:get-vague-object system target-name))
         (slot-position (netfarm:netfarm-class-computed-slot-position
                         (class-of target) name)))
    (when (null slot-position)
      (return-from netfarm-server:apply-side-effect))
    ;; Append the computed value information to the end of the object file
    (with-named-resource (target-name
                          (filesystem-database-access-table system))
      (with-appending-binary-file (s system target-name "effects")
        (netfarm:binary-render (list 1 slot-position value)
                               writer))
      ;; Append the cause's name to the subject's affected-by file
      (with-appending-binary-file (s system target-name "affected-by")
        (netfarm:binary-render cause-name writer)))
    ;; Append the computed value information to the cause's affects file
    (with-named-resource (cause-name (filesystem-database-access-table system))
      (with-appending-binary-file (s system cause-name "affects")
        (netfarm:binary-render target-name writer)))))

(defmethod netfarm-server:apply-side-effect
    ((system filesystem-database-mixin) cause-name
     (type (eql 'netfarm-scripts:remove-computed-value))
     &key name value target)
  (let* ((target-name   (netfarm:hash-object* target))
         (vague-object  (netfarm-server:get-vague-object system target-name))
         (slot-position (netfarm:netfarm-class-computed-slot-position
                         (class-of target))))
    (when (null slot-position)
      (return-from netfarm-server:apply-side-effect))
    ;; Append the computed value information to the end of the object file
    (with-named-resource (target-name
                          (filesystem-database-access-table system))
      (with-appending-binary-file (s system target-name "effects")
        (netfarm:binary-render (list -1 slot-position value)
                               writer))
      (with-appending-binary-file (s system target-name "affected-by")
        (netfarm:binary-render cause-name writer)))
    ;; Append the computed value information to the cause's affects file
    (with-named-resource (cause-name (filesystem-database-access-table system))
      (with-appending-binary-file (s system cause-name "affects")
        (netfarm:binary-render target-name writer)))))

(defmethod netfarm-server:vague-object-stored-p
    ((system filesystem-database-mixin) name)
  (probe-file (object-pathname system name "object")))

(defmethod netfarm-server:objects-affected-by ((system filesystem-database-mixin) name)
  (with-open-binary-file (s system name "affects")
    (let ((names (make-hash-table :test 'equal)))
      (do-until-empty (s)
        (setf (gethash (netfarm:binary-parse reader) names) t))
      (alexandria:hash-table-keys names))))

(defmethod netfarm-server:objects-affecting ((system filesystem-database-mixin) name)
  (with-open-binary-file (s system name "affected-by")
    (let ((names (make-hash-table :test 'equal)))
      (do-until-empty (s)
        (setf (gethash (netfarm:binary-parse reader) names) t))
      (alexandria:hash-table-keys names))))
