(in-package :netfarm-server-filesystem-database)

(defun make-string-set ()
  (make-hash-table :test 'equal :rehash-size 2.0))

(defun write-string-set (set stream)
  (flet ((writer (sequence)
           (write-sequence sequence stream)))
    (maphash (lambda (name value)
               (declare (ignore value))
               (netfarm:binary-render name #'writer))
             set)))
(defun read-string-set (stream)
  (flet ((reader (byte)
           (read-byte byte stream)))
    (let ((set (make-string-set)))
      (do-until-empty (stream)
        (let ((name (netfarm:binary-parse #'reader)))
          (assert (stringp name))
          (setf (gethash name set) t)))
      set)))

(defgeneric load-string-set (system slot-name pathname set-name)
  (:method ((system filesystem-database-mixin) slot-name pathname set-name)
    (let ((set-pathname
            (system-relative-pathname system pathname)))
      (cond
        ((probe-file set-pathname)
         (with-open-file (stream set-pathname
                                 :direction :input
                                 :element-type '(unsigned-byte 8))
           (setf (slot-value system slot-name)
                 (box (read-string-set stream)))))
        (t
         (format *debug-io* "~&~s does not exist; creating a new ~(~a~) set."
                 set-pathname set-name)
         (let ((new-set (make-string-set)))
           (setf (slot-value system slot-name)
                 (box new-set))
           (save-string-set system pathname new-set)))))))

(defgeneric save-string-set (system pathname set)
  (:method ((system filesystem-database-mixin) pathname set)
    (with-open-file (stream (system-relative-pathname system pathname)
                            :direction :output
                            :element-type '(unsigned-byte 8)
                            :if-exists :rename)
      (write-string-set set stream))))
