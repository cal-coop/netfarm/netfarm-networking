(in-package :netfarm-server-filesystem-database)

(defclass filesystem-database-mixin ()
  ((access-table :initform (make-named-resource-table)
                 :reader filesystem-database-access-table)
   (dependency-graph :reader filesystem-database-dependency-graph)
   (directory :initarg :directory
              :initform (alexandria:required-argument :directory)
              :reader filesystem-database-directory)
   (vague-objects :initform (concurrent-hash-table:make-chash-table :test 'equal)
                  :reader filesystem-database-vague-objects)
   (unpresentable-set :reader filesystem-database-unpresentable-set)
   (interesting-set   :reader filesystem-database-interesting-set)))

(defclass filesystem-system (filesystem-database-mixin
                             netfarm-server:dependency-table-mixin
                             netfarm-server:netfarm-system)
  ())

(defmethod initialize-instance :after ((system filesystem-database-mixin) &key)
  (flet ((fix-pathname (pathname)
           (make-pathname :directory (append (pathname-directory pathname)
                                             (list (pathname-name pathname))))))
    (unless (null (pathname-name (filesystem-database-directory system)))
      (let* ((pathname (filesystem-database-directory system))
             (fixed-pathname (ignore-errors (fix-pathname pathname))))
        (if (null fixed-pathname)
            (error "~s isn't a directory." pathname)
            (progn
              (with-simple-restart (dwim "Pretend you used the pathname ~s"
                                         fixed-pathname)
                (error "~s isn't a directory." pathname))
              (format *query-io* "~&;; Remember to update your code to instantiate the system with
;; :directory ~s"
                      fixed-pathname)
              (setf (slot-value system 'directory) fixed-pathname)))))))

(defmethod decentralise-system:start-system :before
    ((system filesystem-database-mixin))
  (load-dependency-graph system)
  (load-unpresentable-set system)
  (load-interesting-set system))

(defmethod decentralise-system:stop-system :after
    ((system filesystem-database-mixin))
  (save-dependency-graph system)
  (save-unpresentable-set system)
  (save-interesting-set system))

#+(or)
(defmethod netfarm-server:compute-threads append
    ((system filesystem-database-mixin))
  (list 'periodically-save-dependency-graph))
