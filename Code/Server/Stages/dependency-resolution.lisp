(in-package :netfarm-server)

(define-time-meter *add-new-vague-object*
  "Adding new vague objects" "netfarm-server stages")
;;; The first step to resolving an object (verb pending) in Netfarm is to
;;; determine all the dependencies that a vague-object has, and retrieve
;;; those that haven't already been retrieved.
;;; (The resolution process is "recursive" in that we have to have resolved
;;;  dependencies as well, but they can be used before scripts have been run.)
(defgeneric add-new-vague-object (system vague-object)
  (:method ((system netfarm-system) vague-object)
    (safe-queue:mailbox-send-message (netfarm-system-vague-objects-to-write system)
                                     vague-object)))

(defun call-with-consistent-blocks (system names continuation)
  (if (null names)
      (funcall continuation)
      (decentralise-standard-system:with-consistent-block ((first names) system)
        (call-with-consistent-blocks system (rest names) continuation))))

(defun vague-object-writing-loop (system)
  (with-received-message (vague-object
                          (netfarm-system-vague-objects-to-write system))
    (with-time-meter (*add-new-vague-object*)
      (let ((dependencies (netfarm:compute-dependencies vague-object))
            (name (netfarm:vague-object-name vague-object)))
        (put-vague-object system vague-object)
        (remove-other-interesting-block system name)
        ;; Ensure that no one provides these while we're reasoning about them.
        (call-with-consistent-blocks
         system (sort (copy-list dependencies) #'string<)
         (lambda ()
           (let ((dependencies (remove-if (lambda (name)
                                            (vague-object-stored-p system name))
                                          dependencies)))
             (if (null dependencies)
                 (mark-as-fulfilled system vague-object)
                 (dolist (dependency dependencies)
                   (add-dependency-edge system :dependency name dependency))))))))))

;;; This thread will take the name of an object which has had all its
;;; dependencies retrieved and remove itself as a dependency for all objects
;;; that depend on it.
(define-time-meter *resolve-dependencies*
  "Resolving dependencies" "netfarm-server stages")
(define-time-meter *verifying-signatures*
  "Verifying object signatures" "netfarm-server")
(defgeneric dependency-resolution-loop (system)
  (:method ((system netfarm-system))
    (with-received-message (vague-object
                            (netfarm-system-fulfilled-objects system))
      (with-time-meter (*resolve-dependencies*)
        (let ((name (netfarm:vague-object-name vague-object)))
          (remove-dependents system name)
          (when (with-time-meter (*verifying-signatures*)
                  (netfarm:verify-vague-object-signatures vague-object))
            (handler-case
                (let ((class (get-class system
                                        (netfarm:vague-object-schema-name vague-object))))
                  (netfarm:apply-class vague-object class))
              (:no-error (object)
                (add-initialization system name object))
              (error (e) (print e)))))))))
