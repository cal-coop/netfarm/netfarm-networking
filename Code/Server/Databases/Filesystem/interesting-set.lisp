(in-package :netfarm-server-filesystem-database)

;;; The interesting block set is represented as a hash table, where other
;;; interesting names are present, and uninteresting names are not.

;;; This grows slower than the presentable set (and can shrink), but will
;;; probably fit in memory, so it will not create any issues.

(defmethod netfarm-server:add-other-interesting-block
    ((system filesystem-database-mixin) name)
  (with-unlocked-box (table (filesystem-database-interesting-set system))
    (setf (gethash name table) t)))

(defmethod netfarm-server:remove-other-interesting-block
    ((system filesystem-database-mixin) name)
  (with-unlocked-box (table (filesystem-database-interesting-set system))
    (remhash name table)))

(defmethod netfarm-server:other-interesting-block-p
    ((system filesystem-database-mixin) name)
  (with-unlocked-box (table (filesystem-database-interesting-set system))
    (gethash name table)))

(defgeneric save-interesting-set (system)
  (:method ((system filesystem-database-mixin))
    (with-unlocked-box (table (filesystem-database-interesting-set system))
      (save-string-set system "interesting-set" table))))

(defgeneric load-interesting-set (system)
  (:method ((system filesystem-database-mixin))
    (load-string-set system 'interesting-set
                     "interesting-set" 'interesting)))
