(in-package :netfarm-server)

(defclass netfarm-system (netfarm-kademlia:netfarm-kademlia-format-mixin
			  decentralise-kademlia:kademlia-system-mixin
			  decentralise-standard-system:standard-system)
  ((fulfilled-object-mailbox :initform (safe-queue:make-mailbox)
                             :reader netfarm-system-fulfilled-objects)
   (interpreter-mailbox :initform (safe-queue:make-mailbox)
                        :reader netfarm-system-interpreters)
   (write-mailbox :initform (safe-queue:make-mailbox)
                  :reader netfarm-system-vague-objects-to-write)
   (script-machine-workers :initarg :script-machine-workers
                           :initform 4
                           :reader netfarm-system-script-machine-workers)
   (cached-objects :initform (decentralise-utilities:make-locked-box
                              :value (trivial-garbage:make-weak-hash-table
                                      :test #'equal
                                      :weakness :value))
                   :reader netfarm-system-cached-objects)
   (cached-names   :initform (decentralise-utilities:make-locked-box
                              :value (trivial-garbage:make-weak-hash-table
                                      :test #'eq
                                      :weakness :key))
                   :reader netfarm-system-cached-names)
   (worker-threads :initform '()
                   :accessor netfarm-system-worker-threads)))

(defmethod decentralise-standard-system:system-translation-target
    ((system netfarm-system))
  'netfarm:vague-object)

;;; Vague object storage
(define-condition vague-object-not-found (error)
  ((name :initarg :name :reader vague-object-not-found-name)))
(defgeneric put-vague-object (system vague-object)
  (:documentation "Write a vague object to the system's database."))
(defgeneric get-vague-object (system name)
  (:documentation "Get a vague object (or raise a condition of type vague-object-not-found) from the system's database."))
     
(defgeneric map-vague-objects (function system)
  (:documentation "Call a function repeatedly with each object's name."))
(defgeneric vague-object-stored-p (system name)
  (:documentation "Return true if a vague object with name NAME is stored."))
(defgeneric count-vague-objects (system)
  (:documentation "Return the count of stored vague objects (presentable or otherwise)."))
(defgeneric presentable-name-p (system name)
  (:documentation "Is the vague object named NAME currently presentable (i.e. has it finished being processed?)"))
(defgeneric (setf presentable-name-p) (presentable? system name)
  (:documentation "Set the presentable status of the vague object named NAME."))
(defgeneric count-presentable-objects (system)
  (:documentation "Return the count of presentable vague objects."))

;;; Other interesting block set
(defgeneric add-other-interesting-block (system name))
(defgeneric remove-other-interesting-block (system name))
(defgeneric other-interesting-block-p (system name))

;;; Threads
(defgeneric compute-threads (system)
  (:method-combination append)
  (:method append ((system netfarm-system))
    (append (list 'dependency-resolution-loop)
            (loop repeat (netfarm-system-script-machine-workers system)
                  collect 'run-script-machine)
            (loop repeat 4
                  collect 'vague-object-writing-loop))))
