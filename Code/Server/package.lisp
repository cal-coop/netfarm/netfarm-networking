(defpackage :netfarm-server
  (:use :cl :concurrent-hash-table)
  (:import-from :decentralise-utilities
                #:define-time-meter #:with-time-meter
                #:box #:with-unlocked-box)
  (:export #:netfarm-system #:memory-system #:simple-memory-system
           #:dependency-list-mixin #:dependency-table-mixin #:caching-mixin
           #:put-vague-object #:get-vague-object
           #:get-object #:get-class
           #:map-vague-objects #:vague-object-stored-p
           #:count-vague-objects
           #:vague-object-not-found
           #:presentable-name-p
           #:count-presentable-objects
           #:compute-threads 
           #:add-dependency-edge #:remove-dependents
           #:map-dependencies #:map-dependents
           #:map-dependency-edges #:no-dependencies-p
           #:add-other-interesting-block #:other-interesting-block-p
           #:remove-other-interesting-block
           #:apply-side-effect #:computed-value-counters
           #:objects-affected-by #:objects-affecting))
