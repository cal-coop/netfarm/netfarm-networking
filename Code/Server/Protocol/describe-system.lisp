(in-package :netfarm-server)

(defun count-vague-objects (system)
  (let ((count 0))
    (map-vague-objects (lambda (name signatures)
                         (declare (ignore signatures))
                         (incf name))
                       system)
    count))

(defun count-waiting-objects (system)
  (decentralise-utilities:with-unlocked-box
      (table (netfarm-system-object-statuses system))
    (let ((count 0))
      (maphash (lambda (name status)
                 (declare (ignore name status))
                 (incf count))
               table)
      count)))

(defmethod describe-object ((system netfarm-system) stream)
  (let ((vague-objects (count-vague-objects system))
        (waiting-objects (count-waiting-objects system)))
    (format stream "  ~&~s~%" system)
    (format stream "~&This system has ~d object~:p stored, and is running scripts for ~d object~:p~%"
            vague-objects waiting-objects)))
