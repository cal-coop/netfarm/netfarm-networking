(in-package :netfarm-client)

(defun parse-effect (effect-list)
  (unless (consp effect-list)
    (return-from parse-effect nil))
  (macrolet ((cases-for (value &body body)
               `(alexandria:switch ((first ,value) :test #'string=)
                  ,@(loop for ((name . pattern) . forms) in body
                          collect `(,name
                                    (destructuring-bind ,pattern (rest ,value)
                                      ,@forms))))))
    (cases-for effect-list
      (("add-computed-value" name value target)
       (list 'netfarm-scripts:add-computed-value
             :slot name :value value
             :target target))
      (("remove-computed-value" name value target)
       (list 'netfarm-scripts:remove-computed-value
             :slot name :value value
             :target target)))))

(defvar *vague-object-effect-log-table*
  (box (trivial-garbage:make-weak-hash-table :test 'eq
                                             :weakness :value))
  "A table that stores effects, which must be applied to objects after being contextualised.
It is possible that we receive an effect between receiving the object and its schema, and so we have to put it somewhere while we wait for the schema.")

(defun apply-side-effect-on (target effect client subclient)
  (multiple-value-bind (object subscribed-subclient)
      (find-retrieved-object client target)
    (unless (or (null effect)
                (and (not (eq subclient subscribed-subclient))
                     (not (null subscribed-subclient))))
      ;; Claim this object for this subclient.
      (with-unlocked-box (searching-table (client-searching-instances-table client))
        (remhash target searching-table))
      (set-find-retrieved-object client target object subclient)
      (etypecase object
        (netfarm:object
         (when (member (car effect)
                       '(netfarm-scripts:add-computed-value
                         netfarm-scripts:remove-computed-value))
           (let ((slot (netfarm:find-netfarm-computed-slot
                        (class-of object)
                        (getf (cdr effect) :slot))))
             (setf (getf (cdr effect) :slot) slot)))
         (netfarm-scripts:apply-side-effects-on object (list effect)))
        (netfarm:vague-object
         (with-unlocked-box (table *vague-object-effect-log-table*)
           (push effect
                 (gethash object *vague-object-effect-log-table* '()))))
        (null #| A bogus message? |#)))))

(defun run-effect-from-effect-data (client subclient effect-list)
  (let* ((effect (parse-effect effect-list))
         (target (netfarm:reference-hash (getf (rest effect) :target))))
    (apply-side-effect-on target effect client subclient)))

(defun run-effect-from-counters (client subclient effect-list)
  (let* ((effect `(netfarm-scripts:set-counters :counters ,(rest effect-list)))
         (target (first effect)))
    (apply-side-effect-on target effect client subclient)))

(defun run-effect (client subclient message connection)
  (decentralise-messages:message-case message
    ((:block name ~ ~ data)
     (let ((effect-list
             (decentralise-messages:data->object data
                                                 connection
                                                 'netfarm-decentralise2:netfarm-value)))
       (alexandria:switch (name :test #'string=)
         ("effect-data"
          (run-effect-from-effect-data client subclient effect-list))
         ("counters"
          (run-effect-from-counters client subclient effect-list)))))))

(defun resubscribe-to (client object-hash instance searching-table)
  (setf (gethash object-hash searching-table) instance)
  (decentralise-client:run-away
   (decentralise-client:attempt
    (decentralise-client:do-subclients (subclient client `(:get ,object-hash))
      (decentralise-client:%run (get-real-counters* subclient object-hash)
                                #'decentralise-client:succeed
                                #'decentralise-client:next-subclient))
    (lambda (c)
      (warn "Couldn't find another node to get updates for ~s on: ~a"
            instance c)))))
