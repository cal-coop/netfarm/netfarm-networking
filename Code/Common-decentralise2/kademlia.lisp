(defpackage :netfarm-kademlia
  (:use :cl)
  (:export #:netfarm-kademlia-format-mixin))
(in-package :netfarm-kademlia)

(defclass netfarm-kademlia-format-mixin
    (decentralise-kademlia:kademlia-mixin)
  ())

(defun byte-array-to-integer (byte-array)
  (loop with n = 0
        for byte across byte-array
        do (setf n (logior (ash n 8)
                           byte))
        finally (return n)))

(defmethod decentralise-kademlia:parse-hash-name
    ((kademlia netfarm-kademlia-format-mixin) name)
  (values
   (ignore-errors
    (byte-array-to-integer
     (s-base64:decode-base64-bytes
      (make-string-input-stream name))))))
