(in-package :netfarm-server)

;;; A dependency graph is represented by two hash tables, one mapping
;;; a dependency name to a list of (dependent-name . type), and another
;;; mapping a dependent name to a list of (dependency-name . type). 
(defclass dependency-table-mixin ()
  ((graph-lock :initform (bt:make-lock "Dependency graph lock")
               :reader graph-lock)
   (dependency->dependent-table :initform (make-hash-table :test 'equal)
                                :reader dependency->dependent-table)
   (dependent->dependency-table :initform (make-hash-table :test 'equal)
                                :reader dependent->dependency-table))
  (:documentation "A mixin that implements a dependency graph by storing the edges in hash tables."))

(defun (setf gethash-or-remhash) (new-value key hash-table)
  (if (null new-value)
      (remhash key hash-table)
      (setf (gethash key hash-table) new-value)))
(defun gethash-or-remhash (key hash-table)
  (gethash key hash-table))

(defmacro with-tables ((system) &body body)
  (alexandria:once-only (system)
    `(bt:with-lock-held ((graph-lock ,system))
       (let ((dependency->dependent-table (dependency->dependent-table ,system))
             (dependent->dependency-table (dependent->dependency-table ,system)))
         (declare (ignorable dependency->dependent-table dependent->dependency-table))
         ,@body))))

(defmethod add-dependency-edge ((system dependency-table-mixin) type dependent dependency)
  (with-tables (system)
    (push (cons dependency type)
          (gethash dependent dependent->dependency-table))
    (push (cons dependent type)
          (gethash dependency dependency->dependent-table))
    (values)))

(defmethod remove-dependents ((system dependency-table-mixin) dependency)
  (with-tables (system)
    (let ((dependents (gethash dependency dependency->dependent-table)))
      (remhash dependency dependency->dependent-table)
      (dolist (dependent dependents)
        (alexandria:removef (gethash-or-remhash (car dependent)
                                                dependent->dependency-table)
                            dependency
                            :key #'car
                            :test #'string=))
      (loop for (dependent-name . type) in dependents
            collect (list dependent-name type)))))

(defmethod map-dependents (function (system dependency-table-mixin) type dependency)
  (with-tables (system)
    (dolist (pair (gethash dependency dependency->dependent-table))
      (destructuring-bind (dependent . type*) pair
        (when (or (eql type :all)
                  (eql type type*))
          (funcall function type dependent)))))
  (values))
(defmethod map-dependencies (function (system dependency-table-mixin) type dependent)
  (with-tables (system)
    (dolist (pair (gethash dependent dependent->dependency-table))
      (destructuring-bind (dependency . type*) pair
        (when (or (eql type :all)
                  (eql type type*))
          (funcall function type dependency)))))
  (values))

(defmethod map-dependency-edges (function (system dependency-table-mixin))
  (with-tables (system)
    (loop with table = dependent->dependency-table
          for dependent             being the hash-keys of table
          for (dependencies . type) being the hash-values of table
          do (loop for dependency in dependencies
                   do (funcall function type dependent dependency)))))

(defmethod no-dependencies-p ((system dependency-table-mixin) type dependent)
  (with-tables (system)
    (if (eql type :all)
        (null (gethash dependent dependent->dependency-table))
        (not (find type (gethash dependent dependent->dependency-table)
                   :key #'cdr)))))
